(function () {document.addEventListener('DOMContentLoaded', function () {
    ymaps.ready(init);
    function init(){
        //create a yandex map
        var myMap = new ymaps.Map("map_main-page_1", {
            //coordinates of center map; default: [latitude, longitude]
            center: [47.22298433, 39.71811656],
            //zoom's value: 0 - 19
            zoom: 16,
            //disable navigation
            controls: []
        });

        //create a placemark
        var myPlacemark = new ymaps.Placemark([47.22298433, 39.71811656], {}, {
            preset: 'islands#violetEducationIcon', 
            iconGlyph: 'RGEU',
            iconGlyphColor: 'black'
        }); 

        //disable of scrolling zoom map
        myMap.behaviors.disable('scrollZoom');
        // add geoObject on the map
        myMap.geoObjects.add(myPlacemark);
    }
});
})();
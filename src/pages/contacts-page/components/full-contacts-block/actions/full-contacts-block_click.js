(function () {
    var contacts = document.getElementsByClassName('full-contacts-block__contacts-user-block');

    for (var i = 0 ; i < contacts.length; i++) {
        
        //event listener on click
        contacts[i].addEventListener("click", function(contactsEvent) {
            var contactsItem = this;
            
            //cheking of targeting element
            if (contactsItem == contactsEvent.currentTarget) {
                var contactsItemChildren = contactsItem.nextElementSibling;
                
                //applying new classes for a targeting element and showing of a hidden block
                contactsItem.classList.toggle('full-contacts-block__contacts-user-block_active');
                contactsItemChildren.classList.toggle('full-contacts-block__wrapper-block_show');
            }
        });
    }
})();
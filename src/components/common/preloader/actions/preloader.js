(function () {
    window.addEventListener('load', function () {

        var preloader = document.querySelector('.preloader');
        var timing = 1000;

        window.addEventListener( 'scroll', scrollBlock);

        //function for the end of loading
        setTimeout(function (preloader) {
            preloader.classList.add('preloader_complete');
            window.removeEventListener('scroll', scrollBlock);
        }, timing, preloader);
        
        //function for display transformation of preloader
        setTimeout(function (preloader) {
            preloader.classList.add('preloader_display-blocking');
        }, timing + 600, preloader);
        
    });
})();

function scrollBlock() {
    window.scrollTo(0, 0);
}
$(document).ready(function() {
    // event handler of burger-button clicks
    $('.burger-button').on('click', function(event) {
        event.preventDefault();
        burgerButtonClick();
    });
});

// show/hide main-navigation-panel when click at burger-button
function burgerButtonClick() {
    var $mainNavPanel = $('.main-navigation-panel');

    //toggle class of elements
    $mainNavPanel.toggleClass('main-navigation-panel_media');
    $mainNavPanel.find('.navigation-bar').toggleClass('navigation-bar_media');
    $mainNavPanel.find('.logo').toggleClass('logo_media');
}

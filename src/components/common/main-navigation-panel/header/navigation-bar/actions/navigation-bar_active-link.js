(function () {
    var navBarItem = document.querySelector('.navigation-bar').getElementsByTagName('a');
    var url = document.location.href;

    for(var i = 0; i < navBarItem.length; i++) {
        if (navBarItem[i].href === url) {
            navBarItem[i].className += ' navigation-bar__item_active';
            return;
        };
    };
})();
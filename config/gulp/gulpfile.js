const path = require('path'); 

const gulp = require('gulp'),
sass = require('gulp-sass'),
cache = require('gulp-cache'),
autoprefixer = require('gulp-autoprefixer'),
csso = require('gulp-csso'),
csscomb = require('gulp-csscomb'),
// prettier = require('gulp-prettier'),
wait = require('gulp-wait');

gulp.task('sassConvertDev', function () {
    return gulp.src('../../src/style/*.scss')
    .pipe(wait(200))
    .pipe(sass())
    .pipe(gulp.dest('../../src/css'));
});

gulp.task('sassConvertProd', function () {
    return gulp.src('../../src/style/*.scss')
    .pipe(wait(200))
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 3 version'],
        cascade: true
    }))
    .csso()
    .pipe(gulp.dest('../../public/css'));
});

//TESTING
/*gulp.task('prettierJS', function () {
    return gulp.src('../../src/test/*.js')
    .pipe(prettier({ 
        useTabs: false,
        printWidth: 80,
        tabWidth: 4,
        singleQuote: true
    }))
    .pipe(gulp.dest('../../src/test'));
});*/

gulp.task('watch', function () {
    gulp.watch(['../../src/style/*.scss'], function () {
        setTimeout(function () {
            gulp.start('sassConvertDev');
        }, 100);
    });
});

gulp.task('cacheClear', function() {
    return cache.clearAll();
});